import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Modal,
  Pressable,
} from 'react-native';
import {Screen} from 'react-native-screens';
import {useNavigation} from '@react-navigation/native';

import AsyncStorage from '@react-native-community/async-storage';

export default function Header() {
  const [search, setSearch] = useState('');
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [sodienthoai, setSoDienThoai] = useState('');

  const Login = async () => {
    const storeData = async () => {
      try {
        await AsyncStorage.setItem('token', sodienthoai);
      } catch (e) {
        console.log(e);
        // saving error
      }
    };
    storeData();
    const getData = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
      } catch (e) {
        console.log(e);
        // error reading value
      }
    };
    getData();
  };

  return (
    <View style={styles.container_100}>
      <View
        style={{
          marginHorizontal: 10,
          marginVertical: 5,
          width: 250,
          flexDirection: 'row',
        }}>
        <View style={styles.inputSearch}>
          <Image
            style={styles.logoDTN}
            source={require('../../assets/logo.png')}
          />
          <TextInput
            id="searchKey"
            style={styles.input}
            // onChangeText={text => setKeySearch(text)}
            placeholder="Tìm kiếm ..."
          />
        </View>
      </View>
      <View style={styles.container}>
        <View style={{display: 'none'}}>
          <Image
            style={styles.img_header}
            source={require('../../assets/bell.png')}
          />
          <Text style={styles.img_header_num}>2</Text>
        </View>
        <View style={{marginHorizontal: 8}}>
          <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
            <Image
              style={styles.img_header}
              source={require('../../assets/cart_header.png')}
            />
            <Text style={styles.img_header_num}>1</Text>
          </TouchableOpacity>
        </View>
        <View style={{display: 'none'}}>
          <Image
            style={styles.img_header_margin}
            source={require('../../assets/profile.png')}
          />
        </View>
      </View>
      <View style={styles.centeredView}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
            setModalVisible(!modalVisible);
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>
                Nhấn vào đây để đăng nhập bằng
              </Text>
              <Image source={require('../../assets/icon/logo_ephamarcy.png')} />
              <Text
                style={[
                  styles.modalText,
                  {color: 'red', fontStyle: 'italic', marginVertical: 10},
                ]}>
                Hoặc
              </Text>
              <Text style={styles.modalText}>Số điện thoại đặt hàng</Text>
              <TextInput
                onChangeText={value => setSoDienThoai(value)}
                style={{
                  borderWidth: 1,
                  borderColor: 'green',
                  borderRadius: 50,
                  width: '100%',
                  fontSize: 20,
                  textAlign: 'center',
                }}
                placeholder="Số điện thoại"></TextInput>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'flex-end',
                  gap: 10,
                  marginTop: 20,
                }}>
                <Pressable
                  style={[
                    styles.button,
                    styles.buttonClose,
                    {backgroundColor: 'green'},
                  ]}
                  onPress={() => Login()}>
                  <Text style={styles.textStyle}>Đồng ý</Text>
                </Pressable>
                <Pressable
                  style={[
                    styles.button,
                    styles.buttonClose,
                    {backgroundColor: '#ddd'},
                  ]}
                  onPress={() => setModalVisible(!modalVisible)}>
                  <Text style={[styles.textStyle, {color: '#000'}]}>Đóng</Text>
                </Pressable>
              </View>
            </View>
          </View>
        </Modal>
        <Pressable
          style={[styles.button, styles.buttonOpen]}
          onPress={() => setModalVisible(true)}>
          <Text style={styles.textStyle}>S</Text>
        </Pressable>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container_100: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  img_header: {
    width: 30,
    height: 30,
    position: 'relative',
  },
  img_header_margin: {
    width: 30,
    height: 30,
    position: 'relative',
    marginBottom: 3,
  },
  img_header_num: {
    position: 'absolute',
    lineHeight: 19,
    fontWeight: 'bold',
    right: 0,
    top: -8,
    color: '#fff',
    borderRadius: 100,
    width: 20,
    height: 20,
    textAlign: 'center',
    backgroundColor: 'red',
    fontSize: 12,
  },
  inputSearch: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    position: 'relative',
    borderRadius: 100,
    paddingLeft: 50,
    paddingRight: 40,
    paddingVertical: 7,
    width: '100%',
    height: 40,
    backgroundColor: '#e3e7e4',
  },
  logoDTN: {
    position: 'absolute',
    left: -2,
    zIndex: 2,
    width: 50,
    height: 50,
  },
  iconSearch: {
    position: 'absolute',
    right: 5,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
    width: '100%',
    paddingHorizontal: 25,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 30,
    width: '100%',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 5,
    padding: 15,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: '500',
    textAlign: 'center',
    fontSize: 18,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 20,
    color: '#000',
  },
});
